package com.sun.xsea.xseaapiservice.domain.category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Random;
import java.util.UUID;

@Document(indexName = "categories", type = "category", shards = 2)
@Data
@NoArgsConstructor
public class Category {
    @Id
    private String id;

    private String name;

    private String code;

    public Category(String name) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.code = generateCategoryCode(name);
    }

    public Category(String id, String name, String code) {
        this.id = id;
        this.name = name;
        this.code = code;
    }

    private String generateCategoryCode(String nane){

        final Integer randomRangeNumber = 100000 + new Random().nextInt(899999);
        if(name == null  || name.isEmpty()){
            return "T" +  randomRangeNumber;
        }

        return name.charAt(0) + "" + randomRangeNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
