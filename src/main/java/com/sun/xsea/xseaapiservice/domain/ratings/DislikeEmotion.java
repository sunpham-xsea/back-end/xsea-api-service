package com.sun.xsea.xseaapiservice.domain.ratings;

public class DislikeEmotion extends Emotion {
    public DislikeEmotion(){
        this.type = EmotionType.DISLIKE;
    }
}
