package com.sun.xsea.xseaapiservice.domain.response;

import com.sun.xsea.xseaapiservice.domain.category.Category;
import org.elasticsearch.common.Nullable;

import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.concurrent.Future;

public class BaseResponse {

    @NotEmpty
    public Boolean success;

    @Nullable
    public Object data;

    @Nullable
    public ApiError error;

    public BaseResponse(@NotEmpty Boolean success, Object data, ApiError apiError) {
        this.success = success;
        this.data = data;
        this.error = apiError;
    }
}
