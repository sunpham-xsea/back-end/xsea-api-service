package com.sun.xsea.xseaapiservice.domain.ratings;


import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;
@Document(indexName = "emotion_status", type = "emotion")
@Data
@NoArgsConstructor
public class Emotion {

    protected EmotionType type;
    protected String makerId;
}
