package com.sun.xsea.xseaapiservice.domain.response;

public class ApiError{
    public Integer code;
    public String reason;
    public String message;
}
