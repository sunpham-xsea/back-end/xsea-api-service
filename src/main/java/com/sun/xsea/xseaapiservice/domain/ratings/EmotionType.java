package com.sun.xsea.xseaapiservice.domain.ratings;

public enum EmotionType {

    LIKE(0), DISLIKE(1);

    private final  int value;

    private EmotionType(int value){
        this.value = value;
    }
}
