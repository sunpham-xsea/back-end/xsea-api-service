package com.sun.xsea.xseaapiservice.domain.ratings;

public class LikeEmotion extends Emotion {

    public LikeEmotion(){
        this.type = EmotionType.LIKE;
    }
}
