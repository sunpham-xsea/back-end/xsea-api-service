package com.sun.xsea.xseaapiservice.domain.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.sun.xsea.xseaapiservice.domain.SeaFood;
import com.sun.xsea.xseaapiservice.domain.ratings.DislikeEmotion;
import com.sun.xsea.xseaapiservice.domain.ratings.LikeEmotion;
import org.elasticsearch.common.Nullable;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class
AddSeaFoodRequest {


    @NotEmpty
    public String categoryId;

    @NotEmpty
    public String name;

    @Nullable
    public String description;


    @NotEmpty
    public Integer price;


    @NotEmpty
    public List<String> imageUrls;


    public SeaFood build(){
        return new SeaFood
                .Builder()
                .initId()
                .setCategoryId(categoryId)
                .setName(name)
                .setDescription(description)
                .setImageUrls(imageUrls)
                .setPrice(price)
                .setRatings(0F)
                .setTimeCreated(System.currentTimeMillis())
                .setTotalDislikes(new ArrayList<>())
                .setTotalLikes(new ArrayList<>())
                .build();
    }
}
