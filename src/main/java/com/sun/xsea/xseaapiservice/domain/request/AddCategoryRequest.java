package com.sun.xsea.xseaapiservice.domain.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.sun.xsea.xseaapiservice.domain.category.Category;
import org.elasticsearch.common.Nullable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class AddCategoryRequest {

    @NotEmpty
    public String name;

    public Category build(){
        return new Category(name);
    }

}
