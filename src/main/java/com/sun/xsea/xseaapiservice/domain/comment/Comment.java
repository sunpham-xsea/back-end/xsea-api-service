package com.sun.xsea.xseaapiservice.domain.comment;

import com.sun.xsea.xseaapiservice.domain.ratings.DislikeEmotion;
import com.sun.xsea.xseaapiservice.domain.ratings.LikeEmotion;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.List;
import java.util.UUID;

@Document(indexName = "comment", type = "comment")
@Data
@NoArgsConstructor
public class Comment {

    @Id
    private String id = UUID.randomUUID().toString();
    private LikeEmotion totalLike;
    private DislikeEmotion totalDislikes;
    private Long timeCreated;
    private Long timeUpdated;
    private Content content;
    private List<String> childrenCommentIds;

    public Comment(String id, LikeEmotion totalLike, DislikeEmotion totalDislikes, Long timeCreated, Long timeUpdated, Content content, List<String> childrenCommentIds) {
        this.id = id;
        this.totalLike = totalLike;
        this.totalDislikes = totalDislikes;
        this.timeCreated = timeCreated;
        this.timeUpdated = timeUpdated;
        this.content = content;
        this.childrenCommentIds = childrenCommentIds;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LikeEmotion getTotalLike() {
        return totalLike;
    }

    public void setTotalLike(LikeEmotion totalLike) {
        this.totalLike = totalLike;
    }

    public DislikeEmotion getTotalDislikes() {
        return totalDislikes;
    }

    public void setTotalDislikes(DislikeEmotion totalDislikes) {
        this.totalDislikes = totalDislikes;
    }

    public Long getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(Long timeCreated) {
        this.timeCreated = timeCreated;
    }

    public Long getTimeUpdated() {
        return timeUpdated;
    }

    public void setTimeUpdated(Long timeUpdated) {
        this.timeUpdated = timeUpdated;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public List<String> getChildrenCommentIds() {
        return childrenCommentIds;
    }

    public void setChildrenCommentIds(List<String> childrenCommentIds) {
        this.childrenCommentIds = childrenCommentIds;
    }
}
