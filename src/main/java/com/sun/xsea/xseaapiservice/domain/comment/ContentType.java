package com.sun.xsea.xseaapiservice.domain.comment;

public enum  ContentType {
    IMAGE(0), TEXT(1), SHARE_LINK(2);

    private final int value;
    private ContentType(int value){
        this.value = value;
    }
}
