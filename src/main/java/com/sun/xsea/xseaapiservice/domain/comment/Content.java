package com.sun.xsea.xseaapiservice.domain.comment;

public class Content {

    private Content type;
    private String content;

    public Content(Content type, String content) {
        this.type = type;
        this.content = content;
    }

    public Content getType() {
        return type;
    }

    public void setType(Content type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
