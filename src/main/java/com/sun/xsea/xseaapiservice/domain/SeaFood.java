package com.sun.xsea.xseaapiservice.domain;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.sun.org.apache.bcel.internal.generic.LUSHR;
import com.sun.xsea.xseaapiservice.domain.ratings.DislikeEmotion;
import com.sun.xsea.xseaapiservice.domain.ratings.LikeEmotion;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.List;
import java.util.UUID;

@Document(indexName = "sea_foods", type = "sea_food", shards = 2)
@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SeaFood {

    @Id
    private String id;

    private String categoryId;

    private String name;
    private String code;
    private String description;
    private String countryCode;

    private Long timeCreated;
    private Long timeUpdated;

    private Integer price;
    private String makerId;

    private Float ratings;
    private List<LikeEmotion> totalLikes;
    private List<DislikeEmotion> totalDislikes;

    private List<String> imageUrls;
    private List<String> commentIds;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Long getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(Long timeCreated) {
        this.timeCreated = timeCreated;
    }

    public Long getTimeUpdated() {
        return timeUpdated;
    }

    public void setTimeUpdated(Long timeUpdated) {
        this.timeUpdated = timeUpdated;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getMakerId() {
        return makerId;
    }

    public void setMakerId(String makerId) {
        this.makerId = makerId;
    }

    public Float getRatings() {
        return ratings;
    }

    public void setRatings(Float ratings) {
        this.ratings = ratings;
    }

    public List<LikeEmotion> getTotalLikes() {
        return totalLikes;
    }


    public void setTotalLikes(List<LikeEmotion> totalLikes) {
        this.totalLikes = totalLikes;
    }

    public List<DislikeEmotion> getTotalDislikes() {
        return totalDislikes;
    }

    public void setTotalDislikes(List<DislikeEmotion> totalDislikes) {
        this.totalDislikes = totalDislikes;
    }

    public List<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(List<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    public List<String> getCommentIds() {
        return commentIds;
    }

    public void setCommentIds(List<String> commentIds) {
        this.commentIds = commentIds;
    }

    private SeaFood(Builder builder) {
        this.id = builder.id;
        this.categoryId = builder.categoryId;
        this.code = builder.code;
        this.commentIds = builder.commentIds;
        this.countryCode = builder.countryCode;
        this.description = builder.description;
        this.imageUrls = builder.imageUrls;
        this.makerId = builder.makerId;
        this.name = builder.name;
        this.price = builder.price;
        this.ratings = builder.ratings;
        this.timeCreated = builder.timeCreated;
        this.timeUpdated = builder.timeUpdated;
        this.totalDislikes = builder.totalDislikes;
        this.totalLikes = builder.totalLikes;

    }

    public static class Builder {
        private String id;

        private String categoryId;

        private String name;
        private String code;
        private String description;
        private String countryCode;

        private Long timeCreated;
        private Long timeUpdated;

        private Integer price;
        private String makerId;

        private Float ratings;
        private List<LikeEmotion> totalLikes;
        private List<DislikeEmotion> totalDislikes;

        private List<String> imageUrls;
        private List<String> commentIds;

        public Builder(){

        }

        public Builder initId() {
            this.id = UUID.randomUUID().toString();
            return this;
        }

        public Builder setCategoryId(String categoryId) {
            this.categoryId = categoryId;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setCode(String code) {
            this.code = code;
            return this;
        }

        public Builder setDescription(String description) {
            this.description = description;
            return this;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public Builder setTimeCreated(Long timeCreated) {
            this.timeCreated = timeCreated;
            return this;
        }

        public Builder setTimeUpdated(Long timeUpdated) {
            this.timeUpdated = timeUpdated;
            return this;
        }

        public Builder setPrice(Integer price) {
            this.price = price;
            return this;
        }

        public Builder setMakerId(String makerId) {
            this.makerId = makerId;
            return this;
        }

        public Builder setRatings(Float ratings) {
            this.ratings = ratings;
            return this;
        }

        public Builder setTotalLikes(List<LikeEmotion> totalLikes) {
            this.totalLikes = totalLikes;
            return this;
        }

        public Builder setTotalDislikes(List<DislikeEmotion> totalDislikes) {
            this.totalDislikes = totalDislikes;
            return this;
        }

        public Builder setImageUrls(List<String> imageUrls) {
            this.imageUrls = imageUrls;
            return this;
        }

        public Builder setCommentIds(List<String> commentIds) {
            this.commentIds = commentIds;
            return this;
        }

        public SeaFood build() {
            return new SeaFood(this);
        }
    }
}
