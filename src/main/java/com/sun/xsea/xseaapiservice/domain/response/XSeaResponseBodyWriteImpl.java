package com.sun.xsea.xseaapiservice.domain.response;

import com.sun.xsea.xseaapiservice.util.JsonUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.io.StringWriter;

//
//@Component
//public class XSeaResponseBodyWriteImpl implements Filter {
//
//    @Override
//    public void doFilter(ServletRequest request, ServletResponse response,
//                         FilterChain chain) throws IOException, ServletException {
//        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
//        HttpServletResponseWrapper httpServletRequestWrapper = new HttpServletResponseWrapper(httpServletResponse);
//
//        chain.doFilter(request, httpServletRequestWrapper);
//        StringWriter stringWriter = new StringWriter(httpServletRequestWrapper.getBufferSize());
//        System.out.println(stringWriter.toString());
//    }
//
//}