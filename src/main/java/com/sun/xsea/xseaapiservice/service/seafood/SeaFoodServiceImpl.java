package com.sun.xsea.xseaapiservice.service.seafood;

import com.sun.xsea.xseaapiservice.domain.SeaFood;
import com.sun.xsea.xseaapiservice.repository.SeaFoodRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.concurrent.Future;

@Service
@Qualifier("no_cache_sea_food_service")
public class SeaFoodServiceImpl implements SeaFoodService {


    private final SeaFoodRepository seaFoodRepository;

    @Autowired
    public SeaFoodServiceImpl(SeaFoodRepository seaFoodRepository) {
        this.seaFoodRepository = seaFoodRepository;
    }


    @Override
    public SeaFood addSeaFoodToCategory(SeaFood seaFood) {
        return seaFoodRepository.save(seaFood);
    }

    @Override
    public Future<Optional<SeaFood>> getSeaFood(String id) {
        return AsyncResult.forValue(seaFoodRepository.findById(id));
    }

}
