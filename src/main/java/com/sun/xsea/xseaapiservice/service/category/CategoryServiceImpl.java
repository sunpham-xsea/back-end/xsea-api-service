package com.sun.xsea.xseaapiservice.service.category;


import com.sun.xsea.xseaapiservice.domain.category.Category;
import com.sun.xsea.xseaapiservice.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Future;

@Service
@Qualifier("no_cache_category_service")
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository;

    @Autowired
    public void CategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Future<Category> addCategory(Category category) {
       Boolean isExisted = categoryRepository.findCategoryByName(category.getName()) != null;
       if(isExisted){
           return AsyncResult.forValue(null);
       }else{
           return AsyncResult.forValue(categoryRepository.save(category));
       }
    }

    @Override
    public List<Category> getCategories() {
        List<Category> categories = new ArrayList<>();
        this.categoryRepository.findAll().forEach(categories::add);
        return categories;
    }

    @Override
    public Future<Optional<Category>> getCategoryById(String id) {
        return AsyncResult.forValue(categoryRepository.findById(id));
    }

    @Override
    public Future<Boolean> deleteCategoryById(String id) {
        this.categoryRepository.deleteCategoriesById(id);
        return AsyncResult.forValue(true);
    }


}
