package com.sun.xsea.xseaapiservice.service.seafood;


import com.sun.xsea.xseaapiservice.domain.SeaFood;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.concurrent.Future;

@Service
public interface SeaFoodService {
    SeaFood addSeaFoodToCategory(SeaFood seaFood);

    @Async
    Future<Optional<SeaFood>> getSeaFood(String id);
}
