package com.sun.xsea.xseaapiservice.service.category;

import com.sun.xsea.xseaapiservice.domain.category.Category;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.Future;

@Service
public interface CategoryService {

    @Async
    Future<Category> addCategory(Category category);

    List<Category> getCategories();

    @Async
    Future<Optional<Category>> getCategoryById(String id);

    @Async
    Future<Boolean> deleteCategoryById(String id);
}
