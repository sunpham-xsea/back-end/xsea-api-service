package com.sun.xsea.xseaapiservice;

import com.sun.xsea.xseaapiservice.domain.category.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;

@ServletComponentScan
@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
public class XseaApiServiceApplication {

    @Autowired
    private ElasticsearchOperations es;


    public static void main(String[] args) {
        SpringApplication.run(XseaApiServiceApplication.class, args);
    }

}
