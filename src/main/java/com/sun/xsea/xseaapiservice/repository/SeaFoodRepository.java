package com.sun.xsea.xseaapiservice.repository;

import com.sun.xsea.xseaapiservice.domain.SeaFood;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface SeaFoodRepository extends ElasticsearchRepository<SeaFood, String> {

}
