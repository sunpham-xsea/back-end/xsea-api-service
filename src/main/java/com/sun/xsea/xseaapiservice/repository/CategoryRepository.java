package com.sun.xsea.xseaapiservice.repository;

import com.sun.xsea.xseaapiservice.domain.category.Category;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
public interface CategoryRepository extends ElasticsearchRepository<Category,String> {
    Category findCategoryByName(String name);
    Void deleteCategoriesById(String id);
}
