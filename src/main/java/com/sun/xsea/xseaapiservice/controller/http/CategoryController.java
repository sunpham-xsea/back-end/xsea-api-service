package com.sun.xsea.xseaapiservice.controller.http;

import com.sun.xsea.xseaapiservice.domain.category.Category;
import com.sun.xsea.xseaapiservice.domain.request.AddCategoryRequest;
import com.sun.xsea.xseaapiservice.domain.response.BaseResponse;
import com.sun.xsea.xseaapiservice.service.category.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.Future;

@RestController
public class CategoryController {

    final
    CategoryService categoryService;

    @Autowired
    public CategoryController(@Qualifier("no_cache_category_service") CategoryService categoryService) {
        this.categoryService = categoryService;
    }


    @PostMapping("/category/add")
    Future<Category> addCategory(@RequestBody AddCategoryRequest request){
        return this.categoryService.addCategory(request.build());
    }

    @GetMapping("/category/list")
    BaseResponse getCategories(){
       return new BaseResponse(true, this.categoryService.getCategories(), null);
    }

    @GetMapping("/category/id")
    Future<Optional<Category>> getCategoryById(@RequestParam String id){
        return this.categoryService.getCategoryById(id);
    }

    @DeleteMapping("/category/id")
    Future<Boolean> deleteCategory(@RequestParam String id){
        return this.categoryService.deleteCategoryById(id);
    }
}
