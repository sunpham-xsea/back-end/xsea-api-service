package com.sun.xsea.xseaapiservice.controller.http;

import com.sun.xsea.xseaapiservice.domain.SeaFood;
import com.sun.xsea.xseaapiservice.domain.request.AddSeaFoodRequest;
import com.sun.xsea.xseaapiservice.domain.response.BaseResponse;
import com.sun.xsea.xseaapiservice.service.seafood.SeaFoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.concurrent.Future;

@RestController
public class SeaFoodController {

    private final SeaFoodService seaFoodService;

    @Autowired
    public SeaFoodController(@Qualifier("no_cache_sea_food_service") SeaFoodService seaFoodService) {
        this.seaFoodService = seaFoodService;
    }

    @PostMapping("/seafood/add")
    BaseResponse addSeaFood(@RequestBody AddSeaFoodRequest request){
        System.out.println("request" + request.name);
        return new BaseResponse(true, seaFoodService.addSeaFoodToCategory(request.build()), null);
    }
}
